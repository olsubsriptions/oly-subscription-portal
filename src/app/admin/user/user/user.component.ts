import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';
import { UserService } from 'src/app/adminservices/user.service';
import { NzModalService } from 'ng-zorro-antd';
import { UserS } from 'src/app/class/userS';
import { AuthService } from 'src/app/core/auth.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

    public users = new Array<UserS>();
    public user: any;

    constructor(
        private userService: UserService,
        private authService: AuthService,
        private modalService: NzModalService) { }

    ngOnInit() {
        this.user = this.authService.getUser();
        this.getUsers();
    }

    public getUsers() {
        this.userService.getUsers(this.user.uid).subscribe(
            (instance: any) => {
                this.users = instance;
                console.log(this.users);
            }
        );
    }

}
