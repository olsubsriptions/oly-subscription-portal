import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/adminservices/home.service';
import { Application } from 'src/app/class/application';
import { ApplicationService } from 'src/app/adminservices/application.service';
import { ApplicationBySubscriptionService } from 'src/app/adminservices/applicationBySubscription.service';
import { AppComponent } from 'src/app/class/appComponent';
import { AuthService } from 'src/app/core/auth.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public test: any;
    public user: any;
    public application: Application;
    public component: AppComponent;
    constructor(
        private homeService: HomeService,
        private authService: AuthService,
        private applicationService: ApplicationService,
        private applicationBySubscriptionService: ApplicationBySubscriptionService) { }

    ngOnInit() {
        this.user = this.authService.getUser();
        console.log(this.user);
        // this.bind();
    }

    public getConsole() {
        this.homeService.getConsole(this.user.uid).subscribe(
            (instance: any) => {
                this.test = instance;
                console.log(this.test);
            }
        );
    }

    public createApplication() {
        this.application = new Application();
        this.application.k0872 = 'Paga Pe';
        this.application.k0873 = 'PPE';
        this.application.k0874 = '1.5';
        this.application.k0875 = '1.5';
        this.application.k0876 = '1.0';
        this.application.k0877 = 'https://moneta-dot-subs-001.appspot.com/';
        this.application.k0878 = '8080';
        this.application.k0879 = true;
        this.application.k0880 = 'assets/img/products/pagape.png';

        this.applicationService.createApplication(this.application).subscribe(
            (instance: any) => {
                console.log(instance);
            }
        );
    }

    public createComponent() {
        this.component = new AppComponent();
        this.component.k0871 = '2df6ba93-9de1-4737-8cfd-2e7c33cd3271';
        this.component.c7052 = 'PRINCIPAL';
        this.component.c7053 = true;

        this.applicationService.createComponent(this.component).subscribe(
            (instance: any) => {
                console.log(instance);
                // this.component.c7052 = 'SelfService';
                // this.applicationService.createComponent(this.component).subscribe(
                //     (instance2: any) => {
                //         console.log(instance2);
                //         this.component.c7052 = 'AppMovil';
                //         this.applicationService.createComponent(this.component).subscribe(
                //             (instance3: any) => {
                //                 console.log(instance3);
                //             }
                //         );
                //     }
                // );
            }
        );
    }
    public bind() {
        this.applicationBySubscriptionService.bind().subscribe(
            (instance: any) => {
                console.log(instance);
            }
        );
    }
}
