import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuard } from '../core/auth.guard';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    // canActivate: [AuthGuard]
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        canActivate: [AuthGuard],
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
      },
      {
        path: 'users',
        canActivate: [AuthGuard],
        loadChildren: () => import('./user/user.module').then(m => m.UserModule),
      },
      {
        path: 'applications',
        canActivate: [AuthGuard],
        loadChildren: () => import('./application/application.module').then(m => m.ApplicationModule),
      },
      {
        path: 'billing',
        canActivate: [AuthGuard],
        loadChildren: () => import('./billing/billing.module').then(m => m.BillingModule),
      },
      {
        path: 'support',
        canActivate: [AuthGuard],
        loadChildren: () => import('./support/support.module').then(m => m.SupportModule),
      },
      {
        path: 'setup',
        canActivate: [AuthGuard],
        loadChildren: () => import('./setup/setup.module').then(m => m.SetupModule),
      },
      {
        path: 'account',
        canActivate: [AuthGuard],
        loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
      },
    ]
  },
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(
      routes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule {
}
