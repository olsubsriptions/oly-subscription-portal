import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchaseComponent } from './purchase/purchase.component';
import { ApplicationComponent } from './application/application.component';
import { SpsDetailComponent } from './application-detail/sps-detail/sps-detail.component';
import { TtsDetailComponent } from './application-detail/tts-detail/tts-detail.component';
import { MntDetailComponent } from './application-detail/mnt-detail/mnt-detail.component';
import { PpeDetailComponent } from './application-detail/ppe-detail/ppe-detail.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'shopping'
  },
  {
    path: 'shopping',
    component: PurchaseComponent
  },
  {
    path: 'active',
    component: ApplicationComponent
  },
  {
    path: 'active/mnt/:uid',
    component: MntDetailComponent
  },
  {
    path: 'active/sps/:uid',
    component: SpsDetailComponent
  },
  {
    path: 'active/tts/:uid',
    component: TtsDetailComponent
  },
  {
    path: 'active/ppe/:uid',
    component: PpeDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicationRoutingModule { }
