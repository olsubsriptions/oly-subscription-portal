import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PpeDetailComponent } from './ppe-detail.component';

describe('PpeDetailComponent', () => {
  let component: PpeDetailComponent;
  let fixture: ComponentFixture<PpeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PpeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PpeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
