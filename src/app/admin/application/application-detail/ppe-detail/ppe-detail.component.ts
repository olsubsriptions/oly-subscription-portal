import { Component, OnInit } from '@angular/core';
import { User } from 'firebase';
import { ActivatedRoute } from '@angular/router';
import { ApplicationService } from 'src/app/adminservices/application.service';
import { AppComponent } from 'src/app/class/appComponent';
import { AppComponentType } from 'src/app/class/enums';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/core/auth.service';

@Component({
    selector: 'app-ppe-detail',
    templateUrl: './ppe-detail.component.html',
    styleUrls: ['./ppe-detail.component.scss']
})
export class PpeDetailComponent implements OnInit {

    public user: any;
    public appSubscriptionId: string;
    public appComponents: Array<AppComponent>;
    public componentType = AppComponentType;
    public principalForm: FormGroup;
    public pagapeShortName = 'PPE';
    saveLoading: any;

    constructor(
        private activatedRoute: ActivatedRoute,
        private applicationService: ApplicationService,
        private authService: AuthService,
        private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.user = this.authService.getUser();
        this.activatedRoute.params.subscribe(parameters => {
            console.log(parameters);
            if (!parameters.uid) {
            } else {
                this.appSubscriptionId = parameters.uid;
                this.getPpeComponents(this.appSubscriptionId, this.pagapeShortName);
                this.createPrincipalForm();
                // this.getK1();
                // this.getK2();
                // this.createIvrForm();
                // this.createSelfServiceForm();
                // this.createAppMovilForm();
            }
        });
    }

    public createPrincipalForm() {
        this.principalForm = this.formBuilder.group({
            displayName: [null, [Validators.required]],
            userName: [null, [Validators.required]],
            password: [null, [Validators.required]],
            authorizationUserName: [null, [Validators.required]],
            domain: [null, [Validators.required]],
            port: [null, [Validators.required]],
            portType: [null, [Validators.required]]
        });
    }

    public getPpeComponents(appSubscriptionId: string, appShortName: string) {
        this.applicationService.getAppComponentsByAppSubscriptionId(appSubscriptionId, appShortName).subscribe(
            (instance: any) => {
                this.appComponents = instance;
                if (this.appComponents) {
                    this.appComponents.forEach(appComponent => {
                        if (!appComponent.subsComponents) {
                            switch (appComponent.c7054) {
                                case this.componentType.PRINCIPAL:
                                    // this.ivrFormDisable = false;
                                    // this.ivrForm.enable();
                                    // this.ivrForm.updateValueAndValidity();
                                    break;
                            }
                        } else {
                            switch (appComponent.c7054) {
                                case this.componentType.PRINCIPAL:
                                    // this.ivrFormDisable = true;
                                    // this.ivrForm.disable();
                                    // this.ivrForm.updateValueAndValidity();
                                    break;
                            }
                            // this.processMntData(appComponent);
                        }
                    });
                    console.log(this.appComponents);
                }
            }
        );
    }

    editConfiguration(component: any){

    }

    showConfirm(component: any){

    }

    downloadAgent(){}

}
