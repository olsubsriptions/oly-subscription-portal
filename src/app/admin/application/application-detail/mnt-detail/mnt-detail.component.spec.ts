import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MntDetailComponent } from './mnt-detail.component';

describe('MntDetailComponent', () => {
  let component: MntDetailComponent;
  let fixture: ComponentFixture<MntDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MntDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MntDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
