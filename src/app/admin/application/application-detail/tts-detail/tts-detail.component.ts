import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/class/appComponent';
import { ApplicationService } from 'src/app/adminservices/application.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'firebase';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-tts-detail',
  templateUrl: './tts-detail.component.html',
  styleUrls: ['./tts-detail.component.scss']
})
export class TtsDetailComponent implements OnInit {

  public appSubscriptionId: string;
  public appComponents: Array<AppComponent>;
  public user: any;
  public ttsShortName = 'TTS';

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private applicationService: ApplicationService) { }

  ngOnInit() {
    this.user = this.authService.getUser();
    this.activatedRoute.params.subscribe(parameters => {
      if (!parameters.uid) {
      } else {
        this.appSubscriptionId = parameters.uid;
        this.getSpsComponents(this.appSubscriptionId, this.ttsShortName);
      }
    });
  }

  public getSpsComponents(appSubscriptionId: string, appShortName: string) {
    this.applicationService.getAppComponentsByAppSubscriptionId(appSubscriptionId, appShortName).subscribe(
      (instance: any) => {
        this.appComponents = instance;
        if (this.appComponents) {
            this.appComponents.forEach(appComponent => {
                if (!appComponent.subsComponents) {
                  switch (appComponent.c7054) {
                  }
                } else {
                  switch (appComponent.c7054) {
                  }
                  this.processSpsData(appComponent);
                }
            });
            console.log(this.appComponents);
        }
      }
    );
  }

  public processSpsData(appComponent: AppComponent) {
    switch (appComponent.c7054) {
    }
  }

}
