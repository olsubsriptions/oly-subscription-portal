import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetupRoutingModule } from './setup-routing.module';
import { SetupComponent } from './setup/setup.component';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { TranslateModule } from '@ngx-translate/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  declarations: [SetupComponent],
  imports: [
    CommonModule,
    NgZorroAntdModule,
    FormsModule,
    TranslateModule,
    SetupRoutingModule,
    ReactiveFormsModule
  ]
})
export class SetupModule { }
