import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GenerateRequirementComponent } from './genera-requerimiento/generate-requeriment.component';
import { RequestComponent } from './request/request.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'generaterequeriments'
  },
  {
    path: 'generaterequeriments',
    component: GenerateRequirementComponent
  },
  {
    path: 'requests',
    component: RequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SupportRoutingModule { }
