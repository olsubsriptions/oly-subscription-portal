import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateRequirementComponent } from './genera-requerimiento/generate-requeriment.component';
import { SupportRoutingModule } from './support-routing.module';
import { RequestComponent } from './request/request.component';

@NgModule({
  declarations: [GenerateRequirementComponent, RequestComponent],
  imports: [
    CommonModule,
    SupportRoutingModule
  ]
})
export class SupportModule { }
