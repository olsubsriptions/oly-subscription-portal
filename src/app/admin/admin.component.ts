import { Component, OnInit } from '@angular/core';
import { AuthService } from '../core/auth.service';
import { User } from 'firebase';
import { Router } from '@angular/router';
import { MenuTypes } from '../class/enums';
import { TranslateService } from '@ngx-translate/core';

const MenuItems = [
    {
        label: 'ADMIN.MENU.HOME',
        type: 'menu',
        icon: 'mail',
        url: '/admin/home'
    },
    {
        label: 'ADMIN.MENU.APPLICATIONS',
        type: 'sub',
        icon: 'appstore',
        childrens: [
            {
                label: 'ADMIN.MENU.ACTIVE_APPLICATIONS',
                url: '/admin/applications/active'
            },
            {
                label: 'ADMIN.MENU.BUY_APPLICATIONS',
                url: '/admin/applications/shopping'
            }
        ]
    },
    {
        label: 'ADMIN.MENU.USERS',
        type: 'sub',
        icon: 'user',
        childrens: [
            {
                label: 'ADMIN.MENU.ACTIVE_USERS',
                url: '/admin/users'
            }
        ]
    },
    {
        label: 'ADMIN.MENU.BILLING',
        type: 'sub',
        icon: 'credit-card',
        childrens: [
            {
                label: 'ADMIN.MENU.PAYMENTS',
                url: '/admin/billing/payments'
            }
        ]
    },
    {
        label: 'ADMIN.MENU.SUPPORT',
        type: 'sub',
        icon: 'team',
        childrens: [
            {
                label: 'ADMIN.MENU.GENERATE_REQUIREMENT',
                url: '/admin/support/generaterequeriments'
            },
            {
                label: 'ADMIN.MENU.MY_REQUIREMENTS',
                url: '/admin/support/requests'
            }
        ]
    },
    {
        label: 'ADMIN.MENU.SETTINGS',
        type: 'menu',
        icon: 'setting',
        url: '/admin/setup'
    },
    {
        label: 'ADMIN.MENU.ACCOUNT',
        type: 'menu',
        icon: 'file',
        url: '/admin/account'
    }
];

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
    public menuItems = MenuItems;
    public menuTypes = MenuTypes;
    public selectedLanguage: string;
    isCollapsed = false;
    user: User;

    constructor(
        private authService: AuthService,
        private router: Router,
        private translate: TranslateService) {
        const user = localStorage.getItem('firebase-user');
        if (user) {
            this.user = JSON.parse(user);
        }
        this.getLanguage();
    }

    ngOnInit() {
    }

    logout() {
        this.authService.logout();
    }

    changeLanguage() {
        if (this.selectedLanguage === 'es') {
            this.translate.use('es');
            localStorage.setItem('portal-curr-lang', 'es');
        } else {
            this.translate.use('en');
            localStorage.setItem('portal-curr-lang', 'en');
        }
    }

    getLanguage() {
        if (localStorage.getItem('portal-curr-lang')) {
            this.translate.setDefaultLang(localStorage.getItem('portal-curr-lang'));
            this.translate.use(localStorage.getItem('portal-curr-lang'));
        } else {
            this.translate.setDefaultLang('es');
            this.translate.use('es');
        }
        this.selectedLanguage = this.translate.getDefaultLang();
    }
}
