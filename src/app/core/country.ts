interface Country{
    name: string;
    prefix: string;
    code: string;
}