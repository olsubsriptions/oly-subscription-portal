import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import * as firebase from 'firebase/app';
import { UserFb } from '../class/userFb';
// import 'firebase/auth';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    public landingPage: string[] = ['/'];
    public user: UserFb;
    constructor(
        private authService: AuthService,
        private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        return new Promise((resolve, reject) => {
            firebase.auth().onAuthStateChanged((user: firebase.User) => {
                if (user) {
                    this.user = new UserFb();
                    this.user.displayName = user.displayName;
                    this.user.email = user.email;
                    this.user.emailVerified = user.emailVerified;
                    this.user.photoURL = user.photoURL;
                    this.user.uid = user.uid;
                    this.user.providerData = user.providerData;
                    user.getIdToken().then(token => {
                        this.user.token = token;
                        this.authService.setUser(this.user);
                        resolve(true);
                    });
                } else {
                    this.authService.setUser(null);
                    this.router.navigate(this.landingPage);
                    resolve(false);
                }
            });
        });
    }
}
