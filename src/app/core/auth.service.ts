import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { UserFb } from '../class/userFb';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    user: UserFb = null;
    suscriptionState = false;

    constructor(
        public angularFireAuth: AngularFireAuth,
        public router: Router
    ) {
    }

    login(email: string, password: string) {
        return this.angularFireAuth.auth.signInWithEmailAndPassword(email, password);
    }

    async logout() {
        this.user = null;
        this.suscriptionState = false;
        await this.angularFireAuth.auth.signOut();
    }

    get authenticated(): boolean {
        return this.user !== null;
    }

    public getSuscriptionState(): boolean {
        return this.suscriptionState;
    }

    public setSuscriptionState(suscriptionState: boolean) {
        this.suscriptionState = suscriptionState;
    }

    public getUser(): UserFb {
        return this.user;
    }

    public setUser(user: UserFb) {
        if (user) {
            this.user = user;
        } else {
            this.user = null;
        }
    }
}
