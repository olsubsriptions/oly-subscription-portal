import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app.routing.module';

import {HttpClient, HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import {NZ_I18N, es_ES} from 'ng-zorro-antd';
import {registerLocaleData} from '@angular/common';
import en from '@angular/common/locales/en';
import de from '@angular/common/locales/de';
import es from '@angular/common/locales/es-PE';

registerLocaleData(en);
registerLocaleData(es, 'es');
registerLocaleData(de, 'de');

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {PortalModule} from './portal/portal.module';
import {CommonsModule} from './commons/commons.module';

// FIREBASE
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';

//
import {environment} from '../environments/environment';
import {AdminModule} from './admin/admin.module';
import {AuthService} from './core/auth.service';
// import {AuthGuard} from './core/auth.guard';
import {AngularFireAuthGuard} from '@angular/fire/auth-guard';
import {AuthGuard} from './core/auth.guard';
import { AuthInterceptor } from './interceptor/authInterceptor';
import { LoaderService } from './adminservices/loader.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    //
    PortalModule,
    CommonsModule,
    AdminModule,
    //
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [
    {provide: NZ_I18N, useValue: es_ES},
    AuthService,
    AuthGuard,
    LoaderService,
    // AngularFireAuthGuard
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
