export enum MenuTypes {
    MENU = 'menu',
    SUBMENU = 'sub'
}

export enum AppComponentType {
    IVR = 1,
    SELF_SERVICE = 2,
    APP_MOVIL = 3,
    PRINCIPAL = 4
}

export enum ApplicationCode {
    MONETA = 'MNT',
    SECURITY_PROTECCION_SUITE = 'SPS',
    TRANSLINK_TRANSACTION_SERVICES = 'TTS',
    PAGA_PE = 'PPE'
}

export enum SubsComponent {
    SELF_SERVICE_URL = 1,
    IVR_DISPLAY_NAME = 2,
    IVR_USERNAME = 3,
    IVR_PASSWORD = 4,
    IVR_AUTHORIZATION_USERNAME = 5,
    IVR_DOMAIN = 6,
    IVR_PORT = 7,
    IVR_PORT_TYPE = 8
}

export enum Language {
    SPANISH = 1,
    ENGLISH = 2
}
