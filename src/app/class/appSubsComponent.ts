export class AppSubsComponent {
    y9071: string; // OUID
    w0791: string; // ApplicationSusbcriptionId
    c7051: string; // ApplicationComponentId
    y9072: number; // Ordinal
    y9073: string; // PropertyName
    y9074: string; // ShortName
    y9075: string; // DefaultPropertyValue
    y9076: boolean; // State
}
