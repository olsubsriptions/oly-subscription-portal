export class UserS {
    g6731: string; // ouid
    b4321: string; // suscriptionId
    g6732: string; // username
    g6733: string; // domain
    displayName: string;
    phoneNumber: string;
    email: string;
}
