export class UserFb {
    displayName: string;
    email: string;
    emailVerified: boolean;
    photoURL: string;
    isAnonymous: boolean;
    uid: string;
    providerData: any;
    token: string;
}
