export class ApplicationSubscription {
    w0791: string; // applicationSubscriptionId
    b4321: string; // subscriptionId
    k0871: string; // applicationId
    u4321: string; // appliedProfileId
    w0792: string; // version
    w0793: string; // versionMajor
    w0794: string; // versionMinor
    w0795: string; // assignedApplicationEndpoint
    w0796: string; // securityKey
    w0797: boolean; // applicationData
    w0798: string; // applicationPort
    w0799: string; // applicationShortName
    // aditional data
    k0872: string; // applicationName
    k0880: string; // applicationImage
}
