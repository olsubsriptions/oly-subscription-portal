export class AppInstance {
    i7071: string; // ouid
    i7072: string; // instanceEndpoint
    i7073: string; // port
    i7074: boolean; // state
    i7075: number; // clients
    i7076: string; // dbUser
    i7077: string; // dbPassword
    k0871: string; // applicationId
}
