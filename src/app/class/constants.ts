export class MaxLength {
    public static EMAIL = 50;
    public static PASSWORD = 20;
    public static PHONE = 14;
    public static PHONE_NUMBER = 17;
    public static CODE = 7;
    public static ID_COMPANY = 11;
    public static COMPANY = 50;
    public static LOCATION = 30;
    public static ADDRESS = 50;
    public static NAME = 50;
    public static BANK_ACCOUNT = 17;
    public static BANK_CCI = 20;
    public static PAYMENT_DETAIL = 50;
    public static PAYMENT_AMOUNT = 7;
}

export class Constants {
    public static regxpag = 8;
    public static pagapeCode = 'PPE';
    public static PEN_CODE = 'PEN';
    public static USD_CODE = 'USD';
    public static DECIMAL_PLACES = 2;
    public static AMOUNT_MAX_LENGTH = 7;
    public static DETAIL_MAX_LENGTH = 50;
    public static ACCOUNT_MAX_LENGTH = 17;
    public static CCI_MAX_LENGTH = 4;
    public static USER_MAX_LENGTH = 20;
    public static USERNAME_MAX_LENGTH = 50;
    public static DOMAIN_MAX_LENGTH = 20;
    public static PSSWRD_MAX_LENGTH = 20;
    public static PHONE_MAX_LENGTH = 13;
    public static CODE_MAX_LENGTH = 7;
    public static MAIL_MAX_LENGTH = 50;
    public static ID_COMPANY_MAX_LENGTH = 11;
    public static LOCATION_MAX_LENGTH = 30;
    public static ADDRESS_MAX_LENGTH = 50;
    public static COMPANY_MAX_LENGTH = 50;
    public static UNIQUE_PAYMENTLINK = 1;
    public static MULTIPLE_PAYMENTLINK = 2;
}

export class RegExpresion {
    public static PAYMENT_LINK_AMOUNT = /^[0-9]+\.{0,1}([0-9]{0,2})$/;
    public static PAYMENT_LINK_DETAIL = /^[A-Za-z0-9.\s,áéíóú]+$/;
    public static SETUP_ACCOUNT_NUMBER = /^[0-9]{0,17}$/;
    public static SETUP_ACCOUNT_CCI = /^[0-9]{0,4}$/;
    public static COMMONS_USER = /^[A-Za-z0-9._-]+$/;
    public static COMMONS_DOMAIN = /^[A-Za-z0-9.]+$/;
    public static COMMONS_PSSWRD = /^[A-Za-z0-9.{}#$%&@]+$/;
    public static COMMONS_NUMBER = /^[0-9]+$/;
    public static COMMONS_MAIL = /^[A-Za-z0-9@._-]+$/;
    public static COMMONS_LOCATION = /^[A-Za-z.\sáéíóú-]+$/;
    public static COMMONS_ADDRESS = /^[A-Za-z0-9.\sáéíóú-]+$/;
    public static COMMONS_COMPANY = /^[A-Za-z0-9.\sáéíóú-]+$/;
    public static COMMONS_USERNAME = /^[A-Za-z\sáéíóú]+$/;
}

export class SpecialKeys {
    public static SPECIAL_KEYS = ['Backspace', 'Tab', 'End', 'Home'];
}

export class AuthenticationType {
    public static EMAIL = 1;
    public static DEFAULT = 2;
}