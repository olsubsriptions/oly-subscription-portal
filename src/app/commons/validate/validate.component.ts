import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { SubscriptionsCommonsService } from '../../core/subscriptions-commons.service';
import { UserService } from 'src/app/adminservices/user.service';
import { AuthService } from 'src/app/core/auth.service';
import { NzModalService, NzNotificationService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { RegExpresion, SpecialKeys, Constants, AuthenticationType } from 'src/app/class/constants';
import { ForgotService } from '../forgot/forgot.service';

@Component({
    selector: 'app-validate',
    templateUrl: './validate.component.html',
    styleUrls: ['./validate.component.scss']
})
export class ValidateComponent implements OnInit {

    validateForm: FormGroup;
    submitted = false;
    disabled = false;
    errorMessage: string;
    passwordVisible: boolean;
    forgotPage: string[] = ['/commons/forgot/password'];
    restoreUserPage: string[] = ['/commons/restore/user'];
    usernameRegex = /^(?=.{4,20}$)(?![._-])(?!.*[._-]{2})[a-zA-Z0-9._-]+(?<![._-])$/;
    domainRegex = /^(?=.{4,20}$)(?![.])(?!.*[.]{2})[a-zA-Z0-9.]+(?<![.])$/;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private subcriptions: SubscriptionsCommonsService,
        private userService: UserService,
        private authService: AuthService,
        private modalService: NzModalService,
        private nzNotification: NzNotificationService,
        private forgotService: ForgotService,
        private translate: TranslateService) {
    }

    ngOnInit() {
        this.validateForm = this.fb.group({
            username: [null, [Validators.required, Validators.pattern(this.usernameRegex)]],
            domain: [null, [Validators.required, Validators.pattern(this.domainRegex)]],
            password: [null, [Validators.required, Validators.minLength(6)]]
        });
    }

    submitForm(): void {
        this.disabled = true;
        this.submitted = true;
        this.errorMessage = null;
        if (this.validateForm.valid) {
            //
            const loginData = {
                username: this.username.value,
                domain: this.domain.value,
                authType: AuthenticationType.EMAIL
            };
            this.subcriptions.getUserName(loginData)
                .subscribe((res) => {
                    if (res && res.status && res.user) {
                        this.userService.loginValidate(res.user).subscribe(
                            (instance: any) => {
                                if (instance.status) {
                                    this.authService.login(res.user, this.password.value)
                                        .then((credentials) => {
                                            this.router.navigate(['/admin']).then();
                                        })
                                        .catch((error) => {
                                            this.errorMessage = 'ERROR.' + (error.code || 'UNKNOWN');
                                        }
                                        ).finally(() => {
                                            this.enableButton();
                                        });
                                } else {
                                    this.modalService.warning({
                                        nzTitle: this.translate.instant('MODAL.WARNING'),
                                        nzContent: this.translate.instant('MODAL.EMAIL_VERIFICATION_MESSAGE')
                                    });
                                    this.router.navigate(['/commons']).then();
                                }
                            }
                        );
                    } else if (res && res.message) {
                        this.errorMessage = 'ERROR.' + (res.message || 'UNKNOWN');
                    } else {
                        this.errorMessage = 'ERROR.UNKNOWN';
                    }
                }, (err) => {
                    console.error(err);
                    this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
                }).add(() => {
                    this.enableButton();
                });

            //
        } else {
            for (const field in this.validateForm.controls) {
                if (this.validateForm.controls[field]) {
                    this.validateForm.controls[field].markAsDirty();
                    this.validateForm.controls[field].updateValueAndValidity();
                }
            }
            this.enableButton();
        }
    }

    enableButton(): void {
        setTimeout(() => {
            this.disabled = false;
        }, 2000);
    }

    goForgotPassword() {
        if (this.username.value && this.domain.value) {
            this.forgotService.clearForgotUser();
            this.forgotService.forgotUser.user = this.username.value + '@' + this.domain.value;
            this.router.navigate(this.forgotPage);
        } else {
            this.nzNotification.create(
                'warning',
                this.translate.instant('MODAL.WARNING'),
                this.translate.instant('COMMONS.FORGOT.PASSWORD.NOTIFICATION_MESSAGE.ERROR_USER')
            );
        }
    }

    goRestoreUser() {
        if (this.username.value && this.domain.value) {
            this.forgotService.forgotUser.user = this.username.value + '@' + this.domain.value;
            this.router.navigate(this.restoreUserPage);
        } else {
            this.nzNotification.create(
                'warning',
                this.translate.instant('MODAL.WARNING'),
                this.translate.instant('COMMONS.FORGOT.USER.NOTIFICATION_MESSAGE.ERROR_USER')
            );
        }
    }

    uNoSymbols(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.COMMONS_USER;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.USER_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    dNoSymbols(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.COMMONS_DOMAIN;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.DOMAIN_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    pNoSymbols(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.COMMONS_PSSWRD;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.PSSWRD_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    get username() {
        return this.validateForm.get('username');
    }
    get domain() {
        return this.validateForm.get('domain');
    }
    get password() {
        return this.validateForm.get('password');
    }
}
