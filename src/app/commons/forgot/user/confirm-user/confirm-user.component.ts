import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ForgotService } from '../../forgot.service';
import { UtilService } from 'src/app/adminservices/util.service';
import { NzSizeLDSType } from 'ng-zorro-antd';

@Component({
    selector: 'app-confirm-user',
    templateUrl: './confirm-user.component.html',
    styleUrls: ['./confirm-user.component.scss']
})
export class ConfirmUserComponent implements OnInit {

    private loginPage: string[] = ['/commons'];
    private userRestored: boolean;
    private userValid: boolean;
    public userMail: string;
    public inputSize: NzSizeLDSType = 'large';

    constructor(
        private router: Router,
        private forgotService: ForgotService,
        private utilService: UtilService
    ) { }

    ngOnInit() {
        this.userRestored = this.forgotService.forgotUser.restored;
        this.userValid = this.forgotService.forgotUser.valid;
        this.userMail = this.utilService.hideData(this.forgotService.forgotUser.email);
        if (!this.userRestored || !this.userValid) {
            this.router.navigate(this.loginPage);
        }
    }

    public goLogin() {
        this.forgotService.clearForgotUser();
        this.router.navigate(this.loginPage);
    }
}
