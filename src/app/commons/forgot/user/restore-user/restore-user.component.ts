import { Component, OnInit } from '@angular/core';
import { ForgotService } from '../../forgot.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { NzSizeLDSType, NzNotificationService, NzModalService } from 'ng-zorro-antd';
import { RegExpresion, SpecialKeys, Constants, AuthenticationType } from 'src/app/class/constants';
import { UserService } from 'src/app/adminservices/user.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-restore-user',
    templateUrl: './restore-user.component.html',
    styleUrls: ['./restore-user.component.scss']
})
export class RestoreUserComponent implements OnInit {

    public userUid: string;
    public userName: string;
    public userEmail: string;
    public restoreForm: FormGroup;
    public inputSize: NzSizeLDSType = 'large';
    public loginPage: string[] = ['/commons'];
    public nextPage: string[] = ['/commons/restore/user/confirm'];

    constructor(
        private forgotService: ForgotService,
        private router: Router,
        private userService: UserService,
        private nzNotification: NzNotificationService,
        private translate: TranslateService,
        private modalService: NzModalService,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit() {
        this.createRestoreForm();
        this.userUid = this.forgotService.forgotUser.uid;
        this.userName = this.forgotService.forgotUser.user;
        if (!this.userName) {
            this.router.navigate(this.loginPage);
        } else {
            this.getUserInfo(this.userName);
        }
    }

    public createRestoreForm() {
        this.restoreForm = this.formBuilder.group({
            email: [null, [Validators.required, Validators.maxLength(50), Validators.minLength(4), Validators.email]],
        });
    }

    public getUserInfo(userName: string) {

        const userDto: any = {
            username: userName,
            authType: AuthenticationType.EMAIL,
        };
        
        this.userService.userInfo(userDto).subscribe(
            (instance: any) => {
                if (instance) {
                    this.forgotService.forgotUser.uid = instance.g6731;
                    this.forgotService.forgotUser.email = instance.email;
                    this.userUid = instance.g6731;
                    this.userEmail = instance.email;
                } else {
                    this.nzNotification.create(
                        'warning',
                        this.translate.instant('MODAL.WARNING'),
                        this.translate.instant('COMMONS.FORGOT.USER.NOTIFICATION_MESSAGE.WARNING_USER')
                    );
                    this.router.navigate(this.loginPage);
                }
            },
            (err) => {
                console.error(err);
                this.router.navigate(this.loginPage);
            },
        );
    }

    mNoSymbols(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.COMMONS_MAIL;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.MAIL_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    public goConfirmUser() {
        if (this.emailValidate()) {
            this.forgotService.forgotUser.valid = true;
            this.userService.userUnlock(this.userUid).subscribe(
                (instance: any) => {
                    if (instance.status) {
                        this.forgotService.forgotUser.restored = true;
                        this.router.navigate(this.nextPage);
                    } else {
                        this.modalService.warning({
                            nzTitle: this.translate.instant('MODAL.WARNING'),
                            nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                        });
                    }
                }
            );
        } else {
            this.nzNotification.create(
                'warning',
                this.translate.instant('MODAL.WARNING'),
                this.translate.instant('COMMONS.FORGOT.USER.NOTIFICATION_MESSAGE.WARNING_EMAIL')
            );
        }
    }

    private emailValidate(): boolean {
        return this.email.value === this.userEmail ? true : false;
    }

    get email() {
        return this.restoreForm.get('email');
    }
}
