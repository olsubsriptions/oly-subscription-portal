import { Component, OnInit } from '@angular/core';
import { ForgotService } from '../../forgot.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { NzSizeLDSType, NzModalService } from 'ng-zorro-antd';
import { RegExpresion, SpecialKeys, Constants } from 'src/app/class/constants';
import { UserService } from 'src/app/adminservices/user.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-restore-password',
    templateUrl: './restore-password.component.html',
    styleUrls: ['./restore-password.component.scss']
})
export class RestorePasswordComponent implements OnInit {

    public userUid: string;
    public userValid: boolean;
    public userName: string;
    public restoreForm: FormGroup;
    public passwordVisible = false;
    public inputSize: NzSizeLDSType = 'large';
    private nextPage: string[] = ['/commons/forgot/password/confirm'];
    private lastPage: string[] = ['/commons/forgot/password/code'];
    private loginPage: string[] = ['/commons'];

    constructor(
        private fb: FormBuilder,
        private forgotService: ForgotService,
        private userService: UserService,
        private modalService: NzModalService,
        private translate: TranslateService,
        private router: Router
    ) { }

    ngOnInit() {
        this.userUid = this.forgotService.forgotUser.uid;
        this.userValid = this.forgotService.forgotUser.valid;
        this.userName = this.forgotService.forgotUser.user;
        console.log(this.forgotService.forgotUser);
        this.createRestoreForm();
        if (!this.userValid) {
            this.router.navigate(this.loginPage);
        }
    }

    public createRestoreForm() {
        this.restoreForm = this.fb.group({
            password: [null, [Validators.required, Validators.minLength(6)]],
            checkPassword: [null, [Validators.required, Validators.minLength(6), this.confirmationValidator]]
        });
    }

    confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
        if (!control.value) {
            return { error: true, required: true };
        } else if (control.value !== this.restoreForm.controls.password.value) {
            return { confirm: true, error: true };
        }
        return {};
    }

    updateConfirmValidator(): void {
        Promise.resolve().then(() => this.checkPassword.updateValueAndValidity());
    }

    pNoSymbols(event: KeyboardEvent | ClipboardEvent, control: AbstractControl) {
        const regex: RegExp = RegExpresion.COMMONS_PSSWRD;
        const specialKeys: Array<string> = SpecialKeys.SPECIAL_KEYS;

        const current: string = control.value ? control.value : '';
        let next: string;
        if (event instanceof KeyboardEvent) {
            if (specialKeys.indexOf(event.key) !== -1) {
                return;
            }
            next = current.concat(event.key);
        } else {
            event.preventDefault();
            return;
        }
        if ((next && !String(next).match(regex)) || (next && next.length > Constants.PSSWRD_MAX_LENGTH)) {
            event.preventDefault();
        }
    }

    goConfirmPassword() {
        this.userService.userChangePass(this.userUid, this.password.value).subscribe(
            (instance: any) => {
                if (instance.status) {
                    this.forgotService.forgotUser.restored = true;
                    this.router.navigate(this.nextPage);
                } else {
                    this.modalService.warning({
                        nzTitle: this.translate.instant('MODAL.WARNING'),
                        nzContent: this.translate.instant('MODAL.WARNING_MESSAGE')
                    });
                }
            }
        );
    }

    public back() {
        this.forgotService.forgotUser.valid = false;
        this.router.navigate(this.lastPage);
    }

    get password() {
        return this.restoreForm.get('password');
    }
    get checkPassword() {
        return this.restoreForm.get('checkPassword');
    }
}
