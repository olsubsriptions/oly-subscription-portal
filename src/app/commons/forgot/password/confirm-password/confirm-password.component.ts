import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ForgotService } from '../../forgot.service';
import { NzSizeLDSType } from 'ng-zorro-antd';

@Component({
    selector: 'app-confirm-password',
    templateUrl: './confirm-password.component.html',
    styleUrls: ['./confirm-password.component.scss']
})
export class ConfirmPasswordComponent implements OnInit {

    private loginPage: string[] = ['/commons'];
    private userRestored: boolean;
    public inputSize: NzSizeLDSType = 'large';

    constructor(
        private router: Router,
        private forgotService: ForgotService
    ) { }

    ngOnInit() {
        this.userRestored = this.forgotService.forgotUser.restored;
        if (!this.userRestored) {
            this.router.navigate(this.loginPage);
        }
    }

    public goLogin() {
        this.forgotService.clearForgotUser();
        this.router.navigate(this.loginPage);
    }
}
