import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/adminservices/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})


export class LoaderComponent implements OnInit {

  isLoading$ = this.loaderService.isLoading.asObservable();
  constructor(
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
  }

}
