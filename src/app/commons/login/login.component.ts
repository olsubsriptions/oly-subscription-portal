import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NzSizeLDSType} from 'ng-zorro-antd';
import {AuthService} from '../../core/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  disabled = false;
  passwordVisible: boolean;
  inputSize: NzSizeLDSType = 'large';

  username: string;
  domain: string;
  user: string;
  // password: string;
  errorMessage: string;

  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService) {
    const data = history.state.data;
    if (!(data && data.user && data.login && data.login.username && data.login.domain)) {
      this.router.navigate(['/', 'commons']).then();
      return;
    } else {
      this.username = data.login.username || '';
      this.domain = data.login.domain || '';
      this.user = data.user || '';
    }
    this.loginForm = this.fb.group({
      password: [null, [Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit(): void {
  }

  submitForm(): void {
    this.disabled = true;
    this.errorMessage = null;
    if (this.loginForm.valid) {
      console.log(this.user, this.loginForm.get('password').value);
      this.authService.login(this.user, this.loginForm.get('password').value)
        .then((credentials) => {
          this.router.navigate(['/admin']).then();
        })
        .catch((error) => {
            console.log(error);
            this.errorMessage = 'ERROR.' + (error.code || 'UNKNOWN');
          }
        ).finally(() => {
        this.enableButton();
      });
    } else {
      for (const field in this.loginForm.controls) {
        if (this.loginForm.controls[field]) {
          this.loginForm.controls[field].markAsDirty();
          this.loginForm.controls[field].updateValueAndValidity();
        }
      }
      this.enableButton();
    }
  }

  enableButton(): void {
    setTimeout(() => {
      this.disabled = false;
    }, 2000);
  }

  back() {
    this.router.navigate(['/commons']).then();
  }
}
