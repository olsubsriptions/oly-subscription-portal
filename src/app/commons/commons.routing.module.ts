import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonsComponent } from './commons.component';
import { ValidateComponent } from './validate/validate.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register/register.component';
import { CompanyComponent } from './register/company/company.component';
import { BillingComponent } from './register/billing/billing.component';
import { ConfirmComponent } from './register/confirm/confirm.component';
import { SendcodeComponent } from './forgot/password/sendcode/sendcode.component';
import { SecuritycodeComponent } from './forgot/password/securitycode/securitycode.component';
import { RestorePasswordComponent } from './forgot/password/restore-password/restore-password.component';
import { ConfirmPasswordComponent } from './forgot/password/confirm-password/confirm-password.component';
import { RestoreUserComponent } from './forgot/user/restore-user/restore-user.component';
import { ConfirmUserComponent } from './forgot/user/confirm-user/confirm-user.component';

const routes: Routes = [
  {
    path: 'commons',
    component: CommonsComponent,
    children: [
      {
        path: '',
        component: ValidateComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'organization',
        component: CompanyComponent
      },
      {
        path: 'billing',
        component: BillingComponent
      },
      {
        path: 'confirm',
        component: ConfirmComponent
      },
      {
        path: 'forgot/password',
        component: SendcodeComponent
      },
      {
        path: 'forgot/password/code',
        component: SecuritycodeComponent
      },
      {
        path: 'forgot/password/restore',
        component: RestorePasswordComponent
      },
      {
        path: 'forgot/password/confirm',
        component: ConfirmPasswordComponent
      },
      {
        path: 'restore/user',
        component: RestoreUserComponent
      },
      {
        path: 'restore/user/confirm',
        component: ConfirmUserComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CommonsRoutingModule {
}
