import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NzSizeLDSType } from 'ng-zorro-antd';
import { RegisterService } from '../register.service';
import { SubscriptionsCommonsService } from 'src/app/core/subscriptions-commons.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  validateForm: FormGroup;
  disabled = false;
  passwordVisible: boolean;
  inputSize: NzSizeLDSType = 'large';
  errorMessage: string;
  usernameRegex = /^(?=.{4,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/;
  phoneNumber = /^[1-9]\d{3,13}$/;
  countries: Country[] = [];
  nextPage: string[] = ['/commons/organization'];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private registerService: RegisterService,
    private commonService: SubscriptionsCommonsService) {
    const state1 = this.registerService.state1 || {};

    this.validateForm = this.fb.group({
      username: [state1.username, [Validators.required, Validators.pattern(this.usernameRegex)]],
      domain: [state1.domain, [Validators.required, Validators.pattern(this.usernameRegex)]],
      email: [state1.email, [Validators.required, Validators.email]],
      country: [, [Validators.required]],
      phoneNumber: [state1.phoneNumber, [Validators.required, Validators.pattern(this.phoneNumber)]],
      password: [, [Validators.required, Validators.minLength(6)]],
      checkPassword: [, [Validators.required, Validators.minLength(6), this.confirmationValidator]],
    });
  }

  ngOnInit() {
    this.disabled = true;
    this.commonService.getCountryCodes()
      .subscribe(
        res => {
          this.countries = res;
        },
        err => {
          console.error(err);
          this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
        },
        () => {
          this.validateForm.controls.country.setValue(this.countries[0]);
          this.enableButton(0);
        }
      );
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  }

  submitForm(): void {
    this.disabled = true;
    this.errorMessage = null;



    if (this.validateForm.valid) {
      const loginData = this.validateForm.getRawValue();
      this.registerService.preValidateLoginData(loginData)
        .subscribe(
          res => {
            if (res && res.status) {
              this.registerService.state1 = loginData;
              this.router.navigate(this.nextPage).then();
            } else if (res && res.message) {
              this.errorMessage = 'ERROR.' + (res.message || 'UNKNOWN');
            } else {
              this.errorMessage = 'ERROR.UNKNOWN';
            }
          },
          err => {
            console.error(err);
            this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
            this.enableButton();
          },
          () => {
            this.enableButton();
          }
        );
    } else {
      for (const field in this.validateForm.controls) {
        if (this.validateForm.controls[field]) {
          this.validateForm.controls[field].markAsDirty();
          this.validateForm.controls[field].updateValueAndValidity();
        }
      }
    }
  }

  enableButton(millisecons: number = 2000): void {
    setTimeout(() => {
      this.disabled = false;
    }, millisecons);
  }

  updateConfirmValidator(): void {
    Promise.resolve().then(() => this.validateForm.controls.checkPassword.updateValueAndValidity());
  }

  CancelRegistration() {
    this.registerService.clearStates();
  }
}
