import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzSizeLDSType } from 'ng-zorro-antd';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {
  billingDataForm: FormGroup;
  disabled = false;
  phoneNumber = /^[1-9]\d{3,13}$/;
  inputSize: NzSizeLDSType = 'large';
  firstState: any = {};
  errorMessage: string;
  copyData = true;

  constructor(private fb: FormBuilder, private router: Router, private registerService: RegisterService) {
    const state4 = this.registerService.state4 || {};
    this.billingDataForm = this.fb.group({
      name: [state4.name, [Validators.required]],
      email: [state4.email, [Validators.email, Validators.required]],
      name2: [state4.name2, [Validators.required]],
      email2: [state4.email2, [Validators.email, Validators.required]],
    });
  }

  ngOnInit() {
    this.firstState = this.registerService.state1;
    if (!this.firstState) {
      this.back();
      return;
    }
  }

  submitForm(): void {
    this.disabled = true;
    this.errorMessage = null;
    if (this.copyData) {
      this.billingDataForm.controls.name2.setValue(this.billingDataForm.controls.name.value);
      this.billingDataForm.controls.email2.setValue(this.billingDataForm.controls.email.value);
    }
    if (this.billingDataForm.valid) {
      this.createUser();
    } else {
      for (const field in this.billingDataForm.controls) {
        if (this.billingDataForm.controls[field]) {
          this.billingDataForm.controls[field].markAsDirty();
          this.billingDataForm.controls[field].updateValueAndValidity();
        }
      }
      this.enableButton();
    }
  }

  createUser() {
    this.disabled = true;
    this.errorMessage = null;
    this.registerService.state4 = this.billingDataForm.getRawValue();
    if (!this.registerService.validateStates()) {
      this.back();
      return;
    }
    this.registerService.createUser()
      .subscribe((res) => {
        if (res && res.status) {
          this.router.navigate(['/commons/confirm']).then();
        } else if (res && res.message) {
          this.errorMessage = 'ERROR.' + (res.message || 'UNKNOWN');
        } else {
          this.errorMessage = 'ERROR.UNKNOWN';
        }
      }, (err) => {
        console.error(err);
        this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
      }, () => {
        this.enableButton();
      });
  }

  enableButton(): void {
    setTimeout(() => {
      this.disabled = false;
    }, 2000);
  }

  skip() {
    this.registerService.state4 = null;
    this.createUser();
  }

  back() {
    this.router.navigate(['/commons/organization']).then();
  }
}
