import {NgModule} from '@angular/core';

import {SharedModule} from '../core/shared.module';

import {CommonsRoutingModule} from './commons.routing.module';

import {ValidateComponent} from './validate/validate.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register/register.component';
import {CompanyComponent} from './register/company/company.component';
import {BillingComponent} from './register/billing/billing.component';
import {ConfirmComponent} from './register/confirm/confirm.component';

import {CommonsComponent} from './commons.component';
import { LoaderComponent } from './loader/loader.component';
import { SendcodeComponent } from './forgot/password/sendcode/sendcode.component';
import { RestorePasswordComponent } from './forgot/password/restore-password/restore-password.component';
import { ConfirmPasswordComponent } from './forgot/password/confirm-password/confirm-password.component';
import { SecuritycodeComponent } from './forgot/password/securitycode/securitycode.component';
import { RestoreUserComponent } from './forgot/user/restore-user/restore-user.component';
import { ConfirmUserComponent } from './forgot/user/confirm-user/confirm-user.component';
import { DirectivesModule } from '../directives/directives.module';

@NgModule({
  declarations: [
    ValidateComponent,
    LoginComponent,
    RegisterComponent,
    CommonsComponent,
    CompanyComponent,
    BillingComponent,
    ConfirmComponent,
    LoaderComponent,
    SendcodeComponent,
    RestorePasswordComponent,
    ConfirmPasswordComponent,
    SecuritycodeComponent,
    RestoreUserComponent,
    ConfirmUserComponent],
  exports: [LoaderComponent],
  imports: [
    SharedModule,
    CommonsRoutingModule,
    DirectivesModule
  ]
})
export class CommonsModule {
}
