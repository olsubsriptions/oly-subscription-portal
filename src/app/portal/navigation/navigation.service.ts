import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {MenuItem} from './navigation.model';



@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  menuData: MenuItem[];

  constructor(private translate: TranslateService) {
    this.menuData = [
      {
        title: 'PRINCIPAL.MENU.SOLUTIONS',
        elementId: 'portal-products',
        // child: [
        //   {
        //     title: 'Moneta Cloud',
        //     elementId: 'portal-products'
        //   },
        //   {
        //     title: 'Sps',
        //     elementId: 'portal-products'
        //   },
        //   {
        //     title: 'Translink',
        //     elementId: 'portal-products'
        //   },
        // ]
      },
      {
        title: 'PRINCIPAL.MENU.CUSTOMERS',
        elementId: 'portal-customers'
      },
      {
        title: 'PRINCIPAL.MENU.COMMUNITY',
        elementId: 'portal-community'
      },
      {
        title: 'PRINCIPAL.MENU.PRICES',
        elementId: 'portal-prices'
      }
    ];
  }

  getData(): MenuItem[] {
    return this.translateItems(this.menuData);
  }

  private translateItems(items: MenuItem[]): MenuItem[] {
    if (!items) {
      return null;
    }

    const response: MenuItem[] = [];
    items.forEach((item, $i) => {
      this.translate.get(item.title).subscribe((res: string) => {
        response[$i] = {
          title: res,
          elementId: item.elementId,
          child: this.translateItems(item.child)
        };
      });
    });

    return response;
  }
}
