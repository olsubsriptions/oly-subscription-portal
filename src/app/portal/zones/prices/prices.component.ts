import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-prices',
  templateUrl: './prices.component.html',
  styleUrls: ['./prices.component.scss']
})
export class PricesComponent implements OnInit {

  data = [
    {
      title: 'PRINCIPAL.PRICES.SUBTITLE_1',
      icon: 'lock',
      content: 'PRINCIPAL.PRICES.DESC_1',
      ref: 'http://www.olympus.pe'
    },
    {
      title: 'PRINCIPAL.PRICES.SUBTITLE_2',
      icon: 'dollar',
      content: 'PRINCIPAL.PRICES.DESC_2',
      ref: 'http://www.olympus.pe'
    },
    {
      title: 'PRINCIPAL.PRICES.SUBTITLE_3',
      icon: 'plus-square',
      content: 'PRINCIPAL.PRICES.DESC_3',
      ref: 'http://www.olympus.pe'
    },
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
