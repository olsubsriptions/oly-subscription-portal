import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationType } from 'src/app/class/constants';
import { SubscriptionsCommonsService } from '../../../core/subscriptions-commons.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  validateForm: FormGroup;
  submitted = false;
  disabled = false;
  errorMessage: string;

  constructor(private fb: FormBuilder, private router: Router, private subscriptions: SubscriptionsCommonsService) {
  }

  ngOnInit() {
    this.validateForm = this.fb.group({
      username: [null, [Validators.required]],
      domain: [null, [Validators.required]]
    });
  }

  submitForm(): void {
    this.disabled = true;
    this.submitted = true;
    this.errorMessage = null;
    if (this.validateForm.valid) {

      const loginData = {
        username: this.validateForm.get('username').value,
        domain: this.validateForm.get('domain').value,
        authType: AuthenticationType.EMAIL
    };
      //
      // const validateFormData = this.validateForm.getRawValue();

      this.subscriptions.getUserName(loginData).subscribe({
        next: res => {
          if (res && res.status && res.user) {
            this.router.navigate(
              ['/commons/login'],
              {
                state: {
                  data: {
                    user: res.user,
                    login: loginData
                  }
                }
              }).then();
          } else if (res && res.message) {
            this.errorMessage = 'ERROR.' + (res.message || 'UNKNOWN');
          } else {
            this.errorMessage = 'ERROR.UNKNOWN';
          }
        },
        error: err => {
          this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
          this.enableButton();
        },
        complete: () => { this.enableButton(); },
      });

      // this.subscriptions.getUserName(validateFormData)
      //   .subscribe((res) => {
      //     if (res && res.status && res.user) {
      //       this.router.navigate(
      //         ['/commons/login'],
      //         {
      //           state: {
      //             data: {
      //               user: res.user,
      //               login: validateFormData
      //             }
      //           }
      //         }).then();
      //     } else if (res && res.message) {
      //       this.errorMessage = 'ERROR.' + (res.message || 'UNKNOWN');
      //     } else {
      //       this.errorMessage = 'ERROR.UNKNOWN';
      //     }
      //   }, (err) => {
      //     console.error(err);
      //     this.errorMessage = 'ERROR.' + (err.name || err.message || 'UNKNOWN');
      //   }, () => {
      //     console.log("finally");
      //     this.enableButton();
      //   });

      //
    } else {
      for (const field in this.validateForm.controls) {
        if (this.validateForm.controls[field]) {
          this.validateForm.controls[field].markAsDirty();
          this.validateForm.controls[field].updateValueAndValidity();
        }
      }
      this.enableButton();
    }
  }

  enableButton(): void {
    setTimeout(() => {
      this.disabled = false;
    }, 2000);
  }
}
