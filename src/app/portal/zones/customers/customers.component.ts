import {Component, OnInit} from '@angular/core';

interface ImageItem {
  source: string;
}

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {

  data: ImageItem[] = [
    {
      source: 'assets/img/logos/bcp.png'
    },
    {
      source: 'assets/img/logos/certus.png'
    },
    {
      source: 'assets/img/logos/cibertec.png'
    },
    {
      source: 'assets/img/logos/gmd.png'
    },
    {
      source: 'assets/img/logos/interbank.png'
    },
    {
      source: 'assets/img/logos/scotiabank.png'
    },
    {
      source: 'assets/img/logos/telefonica.png'
    }
  ];

  constructor() {
  }

  ngOnInit() {
  }

}
