import {Component, Input, OnInit} from '@angular/core';
import {LinkItem} from './link-item';

@Component({
  selector: 'app-link-item',
  templateUrl: './link-item.component.html',
  styleUrls: ['./link-item.component.scss']
})
export class LinkItemComponent implements OnInit {
  @Input()
  title: string;
  @Input()
  data: LinkItem[] = [];

  constructor() {
  }

  ngOnInit() {
  }
}
