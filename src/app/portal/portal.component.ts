import {Component, OnInit} from '@angular/core';
import {NavigationService} from './navigation/navigation.service';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';

// import {PerfectScrollbarComponent, PerfectScrollbarConfigInterface, PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.scss']
})
export class PortalComponent implements OnInit {
  isCollapsed = true;
  menuData: any[];

  constructor(private navigation: NavigationService, private translate: TranslateService) {
    translate.setDefaultLang('es');
    if (localStorage.getItem('portal-curr-lang')) {
      translate.use(localStorage.getItem('portal-curr-lang'));
    } else {
      translate.use('es');
    }
  }

  changeLanguage() {
    const currentLang = this.translate.currentLang;
    if (currentLang === 'es') {
      this.translate.use('de');
      localStorage.setItem('portal-curr-lang', 'de');
    } else {
      this.translate.use('es');
      localStorage.setItem('portal-curr-lang', 'es');
    }
    window.location.reload();
  }


  ngOnInit() {
    this.menuData = this.navigation.getData() || [];
  }

  scroll(id?: string) {
    const el = document.getElementById(id || 'portal-welcome');
    if (el) {
      el.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
    }
    this.isCollapsed = true;
  }


}
