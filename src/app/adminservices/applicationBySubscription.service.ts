import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApplicationSubscription } from '../class/applicationSubscription';
import { AppSubsComponent } from '../class/appSubsComponent';

@Injectable({
    providedIn: 'root'
})

export class ApplicationBySubscriptionService {

    private consoleUrl = environment.console.url;

    constructor(private http: HttpClient) {
    }

    public getApplicationsBySubscription(userId: string): Observable<ApplicationSubscription> {
        const endpoint = this.consoleUrl + '/applications/subscriptions'
            + '?userId=' + userId;
        return this.http.get<ApplicationSubscription>(endpoint);
    }

    public getApplicationSubscription(appSubscriptionId: string): Observable<ApplicationSubscription> {
        const endpoint = this.consoleUrl + '/applications/subscriptions'
            + '/' + appSubscriptionId;
        return this.http.get<ApplicationSubscription>(endpoint);
    }

    public createApplicationSuscription(applicationSubscription: ApplicationSubscription): Observable<ApplicationSubscription> {
        const endpoint = this.consoleUrl + '/applications/subscriptions';
        console.log(endpoint);
        console.log(applicationSubscription);
        return this.http.post<ApplicationSubscription>(endpoint, applicationSubscription);
    }

    public createAppSubsComponent(listAppSubsComponent: Array<AppSubsComponent>): Observable<Array<AppSubsComponent>>{
        const endpoint = this.consoleUrl + '/applications/subscriptions'
        + '/' + listAppSubsComponent[0].w0791
        + '/components';
        return this.http.post<Array<AppSubsComponent>>(endpoint, listAppSubsComponent);
    }

    public getK2(appSubscriptionId: string): Observable<any> {
        const endpoint = this.consoleUrl + '/applications/subscriptions/k2'
            + '?appSubscriptionId=' + appSubscriptionId;

        return this.http.get<any>(endpoint);
    }

    public regenerateK2(appSubscriptionId: string): Observable<any> {
        const endpoint = this.consoleUrl + '/applications/subscriptions/regeneratek2'
            + '?appSubscriptionId=' + appSubscriptionId;

        return this.http.get<any>(endpoint);
    }

    public bind(): Observable<any> {
        // const body = {
        //     working_subscription_derived_key: '449a1d20840022ab1adedec77aac32f014031fc49b7add264733082f5404edacdb9d65dbad2ccb81f34fbe10f546dd8598133414524b541a0e952aa27f16095c032f79e33469b0b376cce76062e295c815a16a86579ea9f962e062ec9551c4b897194959d8fa762a9ec20d8f6439b980',
        //     subscription_id: '8a1b468d-0c3d-4817-bcfb-2930ace1ac03'
        // };

        const body = {
            index_key: '010203F5',
            data: '634b0380938f4e298a886f91eb4d4fb7fc1f8d8052843e7ddca3de62dbd6c140b5683b1d18e2896481c363a0376e5531583cdf1a2b777b849e86c20f8faa22412838590094c9527160758465ebaa171c3db92602820d580bc79c53e718f90c2fd0e07fef0aa3861829282e30dacfcef82b6691b1de4ff1aa7c5ba843ea193a437ca7adfe8eb4e0369ba23bf4a8fd797173324086c84771f8ef0acadb794a393d030c97fbcfd5e73196e5297256d7ab29e731ec2f0018ebb2b064887bfb2ca061a867e64f4d14ac0e8de50485684d1a42bc9072c2f3e9e635706a9ddfb88565ae5c8b0e130d8eec2ee74cd416c15f2eadd579d1b04a542cea5ce565e0838995964b6e6d3401e4526aa06232ac9afe518722a6edc092f619c1ab8da2abf1139a2837099df4d155884a0fa7a1f055d0a90bda83bb46c3a402e6443a749ae7c13b523e258ae77098c8ed9ab1dcb47689f9b0'
        }
        const endpoint = this.consoleUrl + '/applications/subscriptions/bind';
        return this.http.post<ApplicationSubscription>(endpoint, body);
    }
}
