import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private consoleUrl = environment.console.url;

    constructor(private http: HttpClient) {
    }

    public getUsers(userId: string): Observable<any> {
        const endpoint = this.consoleUrl + '/users'
            + '?userId=' + userId;
        return this.http.get<any>(endpoint);
    }

    public loginValidate(user: string): Observable<any> {
        const userDto: any = {};
        userDto.username = user;

        const endpoint = this.consoleUrl + '/users/login/validation';

        return this.http.post<any>(endpoint, userDto);
    }

    public userInfo(userDto: any): Observable<any> {
        const endpoint = this.consoleUrl + '/users/info';

        return this.http.post<any>(endpoint, userDto);
    }

    public userUnlock(userUid: string): Observable<any> {
        const userDto: any = {};
        userDto.username = userUid;

        const endpoint = this.consoleUrl + '/users/info/unlock';

        return this.http.post<any>(endpoint, userDto);
    }

    public userChangePass(userUid: string, pass: string): Observable<any> {
        const userDto: any = {};
        userDto.username = userUid;
        userDto.password = pass;

        const endpoint = this.consoleUrl + '/users/info/changePass';

        return this.http.post<any>(endpoint, userDto);
    }
}
