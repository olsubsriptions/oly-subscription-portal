import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Application } from '../class/application';
import { AppComponent } from '../class/appComponent';
import { AppInstance } from '../class/appInstance';

@Injectable({
    providedIn: 'root'
})
export class ApplicationService {
    private consoleUrl = environment.console.url;

    constructor(private http: HttpClient) {
    }

    public createApplication(application: Application): Observable<Application> {
        return this.http.post<Application>(`${this.consoleUrl}/applications`, application);
    }
    public createInstance(appInstance: AppInstance): Observable<AppInstance> {
        const endpoint = this.consoleUrl + '/applications/instances';
        return this.http.post<AppInstance>(endpoint, appInstance);
    }
    public updateInstance(appInstance: AppInstance): Observable<AppInstance> {
        const endpoint = this.consoleUrl + '/applications/instances'
            + '/' + appInstance.i7071;
        return this.http.post<AppInstance>(endpoint, appInstance);
    }

    public getApplications(userId: string): Observable<Array<Application>> {
        return this.http.get<Array<Application>>(`${this.consoleUrl}/applications?userId=${userId}`);
    }

    public createComponent(appComponent: AppComponent): Observable<AppComponent> {
        return this.http.post<AppComponent>(`${this.consoleUrl}/applications/components`, appComponent);
    }

    public getAppComponentsByAppSubscriptionId(appSubscriptionId: string, appShortName: string): Observable<Array<AppComponent>> {
        const endpoint = this.consoleUrl + '/applications/components'
            + '?appShortName=' + appShortName
            + '&appSubscriptionId=' + appSubscriptionId;
        return this.http.get<Array<AppComponent>>(endpoint);
    }

    public getAllApplications(): Observable<Array<Application>> {
        const endpoint = this.consoleUrl + '/applications/all';
        return this.http.get<Array<Application>>(endpoint);
    }
}
